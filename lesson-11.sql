CREATE TABLE IF NOT EXISTS cars  (name varchar(100), price float, model varchar(50), year int);

INSERT INTO cars (name, price, model, year) values ('santa fe', 20000, 'hyundai', 2018);

INSERT INTO cars (name, price, model, year) values ('i30', 15000, 'hyundai', 2019),
('i20', 12000, 'hyundai', 2018),
('Tucson', 20000, 'hyundai', 2013),
('Accent', 18000, 'hyundai', 2020),
('Camry', 24000, 'Toyota', 2017),
('RAV4', 22000, 'Toyota', 2015),
('Land Cruiser', 30000, 'Toyota', 2014);

CREATE TABLE company (name varchar(50) PRIMARY KEY unique, country varchar(100), year_est int);

INSERT INTO cmpany (name, country, year_est) values ('hyundai', 'South Korea', 1967),  ('Toyota', 'Japan', 1937);

ALTER TABLE cars ADD COLUMN IF NOT EXISTS price_uah float DEFAULT 0;

CREATE FUNCTION convert_currency_to_uah()
    RETURNS trigger
AS
$$
    BEGIN
        NEW.price_uah := NEW.price * 27;
        RETURN NEW;
    END
$$
LANGUAGE plpgsql;

CREATE TRIGGER convert_currency
    BEFORE INSERT OR UPDATE ON cars
    FOR EACH ROW
    EXECUTE PROCEDURE convert_currency_to_uah();

INSERT INTO cars (name, price, model, year) values ('X5', 50000, 'BMV', 2020);
INSERT INTO cars (name, price, model, year) values ('Megan', 30000, 'Reno', 2019);

UPDATE cars
    SET price = 35000
    WHERE name = 'santa fe';

